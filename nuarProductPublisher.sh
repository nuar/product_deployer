#!/bin/bash

echo "Parameters: \$1:Catalog, \$2:username, \$3:password, \$4:server, \$5:product yaml file"
apic config:set catalog=$1

apic login --username $2 --password $3 --server $4

apic product:publish $5

tmp=$(mv -v "$5" "${f/.yaml/''}")

result=$(cat api.txt | grep $tmp)
if [[ -z $result ]]
then
	echo "ERROR: Deploying product to the catalog"
	exit 1
else
	echo "Product deployed"
	exit 0
fi